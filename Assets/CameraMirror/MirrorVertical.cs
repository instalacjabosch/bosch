﻿using UnityEngine;
using System.Collections;

public class MirrorVertical : MonoBehaviour
{
    public bool enableMirror = true;

    Material mat;
    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        if (mat == null)
            mat = new Material(Shader.Find("Hidden/Mirror/Vertical"));
        if (enableMirror)
            Graphics.Blit(src, dest, mat);
        else
            dest = src;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightShift))
            enableMirror = !enableMirror;
    }
}