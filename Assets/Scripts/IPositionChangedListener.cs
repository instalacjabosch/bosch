﻿using UnityEngine;

// Interface for position event listeners
public interface IPositionChangedListener : IInputControllerListener
{
    // Handler for new <0,1> camera / text position
    void OnNewPosition(float position);
}