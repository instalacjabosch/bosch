﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Class that handles switching of text on screen 1
/// </summary>
public class TextScreen : SwitchableImage
{
    static TextScreen staticTextSceenPrefab_;
    static TextScreen staticTextSceenPrefab
    {
        get
        {
            if (staticTextSceenPrefab_ == null)
                staticTextSceenPrefab_ = Resources.Load<TextScreen>("TextScreenStaticDesc");
            return staticTextSceenPrefab_;
        }
    }
    
    static DynamicTextScreen dynamicTextSceenPrefab_;
    static DynamicTextScreen dynamicTextSceenPrefab
    {
        get
        {
            if (dynamicTextSceenPrefab_== null)
                dynamicTextSceenPrefab_ = Resources.Load<DynamicTextScreen>("TextScreenDynamicDesc");
            return dynamicTextSceenPrefab_;
        }
    }
    public override void Initialize(BoschState state)
    {
        string resName = state.textPath;

        var sprite = Resources.Load<Sprite>(resName);
        if (sprite != null)
            GetComponent<UnityEngine.UI.Image>().sprite = sprite;
        else
            Debug.LogError("Missing sprite resource " + resName);
    }

    public override SwitchableImage GetPrefab(BoschState state)
    {
        TextScreen prefab;
        if (!state.is3D)
            prefab = staticTextSceenPrefab;
        else
            prefab = dynamicTextSceenPrefab;

        return prefab;
    }

}
