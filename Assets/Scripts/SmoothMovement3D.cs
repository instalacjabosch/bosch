﻿using UnityEngine;
using System.Collections;

// Smooth follow
public class SmoothMovement3D : MonoBehaviour
{
    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;
    public Transform target;

    void Update()
    {
        if (target)
        {
            transform.position = Vector3.SmoothDamp(transform.position, target.position, ref velocity, dampTime);
        }

    }
}