﻿using UnityEngine;
using System.Collections;
using System;

// Class that sets PathMagic position according to InputController events
[RequireComponent(typeof(AudioListener))]
public class AudioManager : MonoBehaviour, IStateChangedListener
{
    // Ranges of IDs for each clip
    // For N clips should contain N+1 values
    // Clip N is played when clipsIdsRanges[N] <= newState.ID < clipsIdsRanges[N+1]
    public int[] clipsIdsRanges;

    // Clips to play on selected states
    public AudioClip[] clips;

    public void Start()
    {
        Debug.Assert(clipsIdsRanges.Length - 1 == clips.Length, "Every clip should have its IDs range, what means clips lenght should be equal to clipsIdsRanges");
        OnNewState(BoschState.initialState);
    }


    AudioClip currentClip = null;

    // Finds proper clip based on given ranges and new state
    AudioClip FindClip(BoschState state)
    {
        for (int i = 0; i < clips.Length; i++)
            if (clipsIdsRanges[i] <= state.id && state.id < clipsIdsRanges[i + 1])
                return clips[i];

        Debug.LogWarning("Failed to find proper clip for state " + state);
        return null;
    }


    bool blendingClips = false;

    // Lenght of clips blending
    public float blendTime = 5.0f;
    // Plays new clip by blending it with current
    void PlayClip(AudioClip clip)
    {
        var source = gameObject.AddComponent<AudioSource>();
        source.clip = clip;
        source.loop = true;
        source.volume = 0;
        source.spatialBlend = 0;
        source.Play();
        blendingClips = true;
    }

    // Clip blending
    // Set volume down for each but last audio source
    // Set volume up for last audio source
    // Remove sources with zero volume
    void Update()
    {
        if(blendingClips)
        {
            var sources = GetComponents<AudioSource>();
            if (sources.Length == 1)
            {
                sources[0].volume = 1;
                blendingClips = false;
            }
            else
            {
                float step = Time.deltaTime / blendTime;
                for (int i = 0; i < sources.Length - 1; i++)
                {
                    sources[i].volume = Mathf.Clamp01(sources[i].volume - step);
                    if (sources[i].volume == 0)
                        Destroy(sources[i]);
                }
                var last = sources[sources.Length - 1];
                last.volume += Mathf.Clamp01(last.volume + step);
            }
        }
    }


    public void OnNewState(BoschState state)
    {
        var clip = FindClip(state);
        if(currentClip == null || currentClip != clip)
        {
            PlayClip(clip);
            currentClip = clip;
        }

        // Disable this listener if 3D scene is loaded
        StartCoroutine(delayedDisableAudioListener(state));
    }

    IEnumerator delayedDisableAudioListener(BoschState state)
    {
        if(state.is3D)
            yield return new WaitForSeconds(0.2f);
        var list = GetComponent<AudioListener>();
        list.enabled = !state.is3D;

        yield return null;
    }

    InputController myController;
    public bool AcceptRegister(InputController controller)
    {
        myController = controller;
        return true;
    }

    void OnDestroy()
    {
        if (myController != null)
            myController.UnregisterListnener(this);
    }

}
