﻿using UnityEngine;
using System.Collections;
using System;

// Class that sets PathMagic position according to InputController events
[RequireComponent(typeof(Jacovone.PathMagic))]
public class PathMagicMovement : MonoBehaviour, IPositionChangedListener
{
    // Component getter
    Jacovone.PathMagic _PM = null;
    Jacovone.PathMagic PM
    {
        get
        {
            if(_PM == null)
                _PM = GetComponent<Jacovone.PathMagic>();
            return _PM;
        }
    }

    // Called before OnStart
    public void OnEnable()
    {
        //Enable PathMagic and set velocity to 0, so transform gets updated
        PM.AutoStart = true;
        PM.VelocityBias = 0;
    }

    InputController myController;
    public bool AcceptRegister(InputController controller)
    {
        myController = controller;
        return true;
    }

    public void OnNewPosition(float position)
    {
        PM.CurrentPos = position;
    }

    void OnDestroy()
    {
        if (myController != null)
            myController.UnregisterListnener(this);
    }
}
