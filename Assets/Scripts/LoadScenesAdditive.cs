﻿using UnityEngine;
using SM = UnityEngine.SceneManagement.SceneManager;

// Script to load scenes once and start
public class LoadScenesAdditive: MonoBehaviour {
        
    public string[] scenes;

    void Start () {
        foreach (string scene in scenes)
            SM.LoadScene(scene, UnityEngine.SceneManagement.LoadSceneMode.Additive);
	}
	
}
