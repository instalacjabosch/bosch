﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class InputController : MonoBehaviour
{
    // Time in seconds for resetting after lack of user's activity
    public float resetTimeout = 120f;

    float timeoutCounter = 0;

    // Use this for initialization
    void Start()
    {
        ListListeners();
    }

    float currentPos = 0;
    BoschState currentState = BoschState.initialState;

    IList<IStateChangedListener> stateListeners = new List<IStateChangedListener>();
    IList<IPositionChangedListener> positionListeners = new List<IPositionChangedListener>();

    void ListListeners()
    {
        var sList = InterfaceHelper.FindObjects<IStateChangedListener>();
        foreach(var sl in sList)
        {
            if (!stateListeners.Contains(sl) &&
                sl.AcceptRegister(this))
                stateListeners.Add(sl);
        }

        var pList = InterfaceHelper.FindObjects<IPositionChangedListener>();
        foreach (var pl in pList)
        {
            if (!positionListeners.Contains(pl) &&
                pl.AcceptRegister(this))
                positionListeners.Add(pl);
        }
    }

    public void UnregisterListnener(IStateChangedListener list)
    {
        if (stateListeners.Contains(list))
            stateListeners.Remove(list);
    }

    public void UnregisterListnener(IPositionChangedListener list)
    {
        if (positionListeners.Contains(list))
            positionListeners.Remove(list);
    }


    void InvokeOnNewPosition(float newPosition)
    {
        ResetTimeoutCounter();

        for (int i = positionListeners.Count - 1; i >= 0; i--)
        {
            var list = positionListeners[i];
            if (list != null)
                list.OnNewPosition(newPosition);
        }
        currentPos = newPosition;

    }

    bool VerifyStateChange(BoschState newState)
    {
        // No change, ignore it
        if (currentState == newState)
            return false;

        // User can return from any other state to initial (reset button)
        if (newState == BoschState.initialState)
            return true;

        // Go to only these 3D scenes that can be loaded
        if(newState.is3D && !Application.CanStreamedLevelBeLoaded(newState.scenePath))
            return false;

        // And these states that has text description
        if (!newState.is3D && Resources.Load(newState.textPath) == null)
            return false;
        
        return true;
    }

    void InvokeOnStateChanged(BoschState newState)
    {
        ResetTimeoutCounter();

        if (!VerifyStateChange(newState))
            return;

        Debug.Log("Changing to state " + newState.id + " " + newState.is3D);

        for(int i = stateListeners.Count - 1; i >= 0; i--) // inverse loop to allow removal of elements inside it
        {
            var list = stateListeners[i];
            if (list != null)
                list.OnNewState(newState);
        }
        currentState = newState;

        ListListeners();

        // Reset position
        currentPos = 0;
    }

    private void ResetTimeoutCounter()
    {
        timeoutCounter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // Update counter of inactivity
        if (currentState != BoschState.initialState)
            timeoutCounter += Time.deltaTime;

        if(resetButton || timeoutCounter > resetTimeout) // reset
        {
            InvokeOnStateChanged(new BoschState(1, false));
        }
        else if (!currentState.is3D)
        {
            if (encoderLeft) // ruch enkoderem w lewo
                InvokeOnStateChanged(new BoschState(currentState.id - 1, false));
            else if (encoderRight) // ruch enkoderem w prawo
                InvokeOnStateChanged(new BoschState(currentState.id + 1, false));
            else if (forwardButton) // dalej
                InvokeOnStateChanged(new BoschState(currentState.id, true));
            //else if (Input.GetKeyDown(KeyCode.DownArrow)) // wstecz
            //    InvokeOnStateChanged(new BoschState(currentState.id, false));
        }
        else
        {
            if (encoderLeft)  // ruch enkoderem w lewo
                InvokeOnNewPosition(Mathf.Clamp01(currentPos - encoderStep));
            else if (encoderRight) // ruch enkoderem w prawo
                InvokeOnNewPosition(Mathf.Clamp01(currentPos + encoderStep));
            else if (backwardButton) // wstecz
                InvokeOnStateChanged(new BoschState(currentState.id, false));
        }
        // TODO - handle serial
    }

    float encoderStep {
        get
        {
            return 0.05f;
        }
    }

    // Was forward button pressed this frame?
    bool forwardButton {
        get {
            return Input.GetKeyDown(KeyCode.UpArrow);
        }
    }

    // Was backward button pressed this frame?
    bool backwardButton{
        get {
            return Input.GetKeyDown(KeyCode.DownArrow);
        }
    }

    // Was reset button pressed this frame?
    bool resetButton{
        get {
            return Input.GetKeyDown(KeyCode.Return);
        }
    }

    // Was encoder turned left this frame?
    bool encoderLeft{
        get {
            return Input.GetKeyDown(KeyCode.LeftArrow);
        }
    }

    // Was encoder turned right this frame?
    bool encoderRight
    {
        get {
            return Input.GetKeyDown(KeyCode.RightArrow);
        }
    }

    public void OnLevelWasLoaded(int level)
    {
        Debug.Log("Level loaded " + level);
        ListListeners();
    }
}
