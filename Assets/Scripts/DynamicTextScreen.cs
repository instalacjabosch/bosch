﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Class that handles movement of text on screen 1
/// </summary>
public class DynamicTextScreen : TextScreen, IPositionChangedListener
{
    // Position in (0,1) range
    float currentPos = 0;

    // Minimal position
    float startY;

    // Maximum position
    float endY;

    // Smooth movement param
    public float dampTime = 0.15f;
    private Vector2 targetAnchoredPosition;

    // For internal use of 
    private Vector2 velocity = Vector2.zero;
    RectTransform _rt = null;

    RectTransform rt
    {
        get
        {
            if (_rt == null)
                _rt = GetComponent<RectTransform>();
            return _rt;
        }
    }

    // Initialization
    public override void Initialize(BoschState state)
    {
        base.Initialize(state);

        //Resize rect to sprite's size
        rt.sizeDelta = GetComponent<UnityEngine.UI.Image>().sprite.rect.size;

        startY = 0;

        float canvasHeight = GetComponentInParent<Canvas>().GetComponent<RectTransform>().sizeDelta.y;
        endY = rt.rect.height - canvasHeight;

        targetAnchoredPosition = rt.anchoredPosition;
    }

    public void OnNewPosition(float position)
    {
        currentPos = position;
        targetAnchoredPosition = new Vector2(rt.anchoredPosition.x, currentPos * (endY - startY) + startY);
    }
    

    protected virtual void Update()
    {
        // Smooth movement
        rt.anchoredPosition = Vector2.SmoothDamp(rt.anchoredPosition, targetAnchoredPosition, ref velocity, dampTime);
    }

    // Unregister from input
    override protected void Unregister()
    {
        base.Unregister();
        if (myController != null)
            myController.UnregisterListnener(this as IPositionChangedListener);
    }
}
