﻿
// State in Bosch installation information architecture diagram
public struct BoschState
{
    readonly int _id;
    readonly bool _is3D;
    
    // Level of depth of state
    public int id
    {
        get { return _id; }
    }

    // Flag if it is 3D scene with description (second column)
    public bool is3D
    {
        get { return _is3D; }
    }

    // Constructor
    public BoschState(int ID, bool Is3D)
    {
        _id = ID;
        _is3D = Is3D;
    }

    // Getter to generate path to scene in Unity assets
    public string scenePath
    {
        get
        {
            return "Scene3D_" + id.ToString("00");
        }
    }

    public string projectorMaskPath
    {
        get
        {
            return "masks/" + id;
        }
    }

    public string textPath
    {
        get
        {
            if (!is3D)
                return "desc_highlight/tekst_maski" + id;
            else
                return "desc_3D/model_" + id;
        }
    }
    public static BoschState initialState
    {
        get
        {
            return new BoschState(1, false);
        }
    }
    #region Equality functions and C# stuff 
    public override bool Equals(object obj)
    {
        if (!(obj is BoschState))
            return false;

        BoschState mys = (BoschState)obj;

        return mys == this;
    }


    public bool Equals(BoschState obj)
    {
        return (id == obj.id) && (is3D == obj.is3D);
    }

    public static bool operator ==(BoschState c1, BoschState c2)
    {
        return c1.Equals(c2);
    }

    public static bool operator !=(BoschState c1, BoschState c2)
    {
        return !c1.Equals(c2);
    }

    public override int GetHashCode()
    {
        unchecked // Overflow is fine, just wrap
        {
            int hash = 17;
            // Suitable nullity checks etc, of course :)
            hash = hash * 23 + id.GetHashCode();
            hash = hash * 23 + is3D.GetHashCode();
            return hash;
        }
    }
    #endregion


}
