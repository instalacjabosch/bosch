﻿using UnityEngine;
using System.Collections;

// Class to keep object looking at target
public class LookAt : MonoBehaviour {
    public GameObject target;
	// Update is called once per frame
	void Update () {
        if(target != null)
            transform.LookAt(target.transform);
	}
}
