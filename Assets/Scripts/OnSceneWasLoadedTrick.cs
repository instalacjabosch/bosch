﻿using UnityEngine;
using SM = UnityEngine.SceneManagement.SceneManager;

// Trick to overcome problem with OnLevelWasLoaded when loading scene additive
// Shall be removed in Unity 5.4
public class OnSceneWasLoadedTrick : MonoBehaviour {
#if UNITY_5_3
    // Use this for initialization
    void Start () {
        var ic = FindObjectOfType<InputController>();
        if (ic != null)
            ic.OnLevelWasLoaded(SM.GetActiveScene().buildIndex);
        Destroy(gameObject);
	}
#else
#error It should be removed and use official way for level callback http://forum.unity3d.com/threads/onlevelwasloaded-not-called-at-all.382939/#post-2489139
#endif
}
