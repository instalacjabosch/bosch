﻿using UnityEngine;
using System.Collections;
using System;

// Class to blend between projector images
public class ProjectorImage : SwitchableImage {

    static ProjectorImage prefab;
    public override SwitchableImage GetPrefab(BoschState state)
    {
        if(prefab == null)
            prefab = Resources.Load<ProjectorImage>("ProjectorImage");
        return prefab;

    }

    // Initialize - load proper texture
    public override void Initialize(BoschState state)
    {
        string resName = state.projectorMaskPath;        
        var sprite = Resources.Load<Sprite>(resName);
        if (sprite != null)
            GetComponent<UnityEngine.UI.Image>().sprite = sprite;
        else
            Debug.LogError("Missing sprite resource " + resName);

    }
}
