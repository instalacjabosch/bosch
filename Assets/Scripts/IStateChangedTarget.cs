﻿using UnityEngine;



// Interface for state changed handlers
public interface IStateChangedListener : IInputControllerListener
{
    /// Handler for new state machine's state
    void OnNewState(BoschState state);
}