﻿// Interface for listeners of InputController events
public interface IInputControllerListener
{
    // Object should return true if it wants to receive input events
    bool AcceptRegister(InputController controller);
}

