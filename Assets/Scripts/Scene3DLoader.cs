﻿using UnityEngine;
using System.Collections;
using SM = UnityEngine.SceneManagement.SceneManager;

// Class that switches 3D scenes according to Bosch state machine
public class Scene3DLoader : MonoBehaviour, IStateChangedListener {
    
    string loadedScene = "";

    public void OnNewState(BoschState state)
    {
        // If anyhthing is loaded - unload it now
        if(loadedScene != "")
            SM.UnloadScene(loadedScene);

        // Ignore non-3D states
        if (state.is3D)
        {
            loadedScene = state.scenePath;
            SM.LoadScene(loadedScene, UnityEngine.SceneManagement.LoadSceneMode.Additive);
        }
        else
            loadedScene = "";
    }


    protected InputController myController;
    public bool AcceptRegister(InputController controller)
    {
        if (controller == null)
            return false;

        myController = controller;
        return true;
    }

    public void OnDestroy()
    {
        Unregister();
    }

    protected virtual void Unregister()
    {
        //Debug.Log(transform.name + " Unregistering");
        if (myController != null)
            myController.UnregisterListnener(this as IStateChangedListener);
    }

}
