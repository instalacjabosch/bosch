﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Canvas))]
[RequireComponent(typeof(UnityEngine.UI.Image))]
public class FadeSceneIn : MonoBehaviour {

    public float fadeTime = 3.0f;

    private float startTime;
	void Start () {
        GetComponent<UnityEngine.UI.Image>().CrossFadeAlpha(0, fadeTime, false);
        Destroy(gameObject,fadeTime);
    }
}
