﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Class that handles switching of image with blending
/// Works by spawning new instance of same prefab with new image
/// </summary>
[RequireComponent(typeof(UnityEngine.UI.Image))]
abstract public class SwitchableImage : MonoBehaviour, IStateChangedListener
{
    [SerializeField]
    float fadeTime = 3.0f;

    // Is this object fading out now?
    protected bool fadingOut = false;

    public abstract void Initialize(BoschState state);
    public abstract SwitchableImage GetPrefab(BoschState state);

    public void OnNewState(BoschState state)
    {
        // Get prefab
        SwitchableImage prefab = Instantiate<SwitchableImage>(GetPrefab(state));

        // Place it in hierarchy
        prefab.transform.SetParent(transform.parent, false);
        prefab.transform.SetAsFirstSibling();
        prefab.Initialize(state);

        // Blend out and destroy after fadeTime
        Destroy(gameObject, fadeTime);
        GetComponent<UnityEngine.UI.Image>().CrossFadeAlpha(0, fadeTime, false);
        Unregister();

        fadingOut = true;

    }


    protected InputController myController;
    public bool AcceptRegister(InputController controller)
    {
        // Don't accept registering in fading out objects
        if (fadingOut || controller == null)
            return false;

        myController = controller;
        return true;
    }

    public void OnDestroy()
    {
        Unregister();
    }

    protected virtual void Unregister()
    {
        //Debug.Log(transform.name + " Unregistering");
        if (myController != null)
            myController.UnregisterListnener(this as IStateChangedListener);
    }
}
