### README ###

Do uruchomienia niezbędne jest Unity 5.3 lub nowsze.
Jako, że aplikacja działa na 3 monitorach to wygodne może być skonfigurowanie 3 okienek Game w odpowiednich rozdzielczościach. Patrz obrazek.

![Capture.PNG](https://bitbucket.org/repo/5kEA5y/images/2559070338-Capture.PNG)

# Rozdzielczości #

 * Display 1 - 2560x1080
 * Display 2 - 1920x1080
 * Display 3 - 1920x1080

# Uruchomienie #
Wystarczy otworzyć scenę bootstrap i dać play.

# Sterowanie #
 * Strzałka w lewo - ruch enkodera w lewo
 * Strzałka w prawo - ruch enkodera w prawo
 * Strzałka w górę - dalej
 * Strzałka w dół - powrót
 * Enter - reset
<<<<<<< HEAD
 * Prawy Shift - odbicie obrazu
 
Brak aktywności przez 120s spowoduje zresetowanie programu (jak wciśnięcie reset).
Czas można dostosowac w scenie bootstrap w obiekcie Input za pomocą wartości ResetTimeout
=======
>>>>>>> origin/julian
 
# Protokół Serial #
 * 1 - ruch enkodera w lewo
 * 2 - ruch enkodera w prawo
 * 3 - dalej
 * 4 - powrót
 * 5 - reset
<<<<<<< HEAD
 
=======

>>>>>>> origin/julian
### Mechanika ###
* Wszystkie zasoby są ładowane dynamicznie i ich obecności zależy zachowanie programu. Ruch góra-dół po maszynie stanu jest możliwy jedynie jeśli istnieje odpowiednia tekstura pod ścieżką Textures/Resources/desc_highlightXX. Analogicznie ruch w prawo - scena 3D jest ładowana tylko jeśli istnieje taka o nazwie Scene3D_XX.
* Wciśnięcia klawiszy i ruch enkodera są traktowane jednakowo i wysyłane do wszystkich automatycznie wylistowanych obiektów. Czyli wystarczy, że obiekt implementuje odpowiedni interfejs *IInputControllerListener* lub *IPositionChangedListener*, a będzie przyjmował zdarzenia od użytkownika.
* Ścieżki do zasobów można zmienić w strukturze BoschState.

### Efekty ###
 * Czas fade'owania można zmienić w prefabach w Prefabs/ w zmiennej Fade Time.
 * Dodanie warunkowego efektu w scenie 3D wymaga jedynie stworzenia dodatkowego skryptu implementującego *IPositionChangedListener* (patrz przykład *PathMagicMovement*).
 * Odbicie lustrzane kamery w pionie wymaga jedynie podpięcia skryptu MirrorVertical. Prawym Shiftem można je włączać / wyłączać.

### Wymiana zaślepek ###
# Tesktury #
* Nie ma tutaj trudności - starczy podmienić pliki tekstur. Trzeba pamiętać, żeby były one typu Sprite oraz warto zmienić im maksymalny rozmiar na 8096.
* Pozycję tekstu w osi X opisującego scenę 3D można dostosować w prefabie TextScreenDynamicDesc.

# Scena 3D #
* Można edytować przykładowe lub stworzyć nową. Ważne, żeby scena zawierała prefab _OnSceneWasLoadedTrick, który obchodzi ograniczenie Unity 5.3. Inaczej obiekty na scenie nie zostaną zarejestrowane do eventów InputControllera.
* Aby starować pozycją PathMagic starczy dodać skrypt PathMagicMovement do obiektu z trajektorią.
* Wygładzenie ruchu w 3D wymaga stworzenia pustego obiektu, który jest przesuwany przez PathMagic oraz kamery która go śledzi. Patrz przykłady.
* Każda scena 3D powinna być zupełnie niezależna - własne światła, itp.
* Niezbędne jest dodanie sceny do budowania w Unity w File->Build Settings.

# Dźwięk #
Żeby zmienić ścieżkę dźwiękową trzeba wybrać nową w obiekcie AudioManager w scenie AudioScene.
Miejsce przełączania klipów można dostosować wartościami ClipsIdsRanges

### TO - DO ###
* Uwzględnienie różnych kroków enkodera dla różnych scen. Aktualnie jest stałe 0.05
* Sprawdzenie czasu ładowania na docelowych scenach. Być może fade-in scen trzeba będzie dostosować.
* Fade-out scen


Artur Ryt, 08.05.2016